---
title: "Overview"
date: 2018-07-24T20:41:03+02:00
draft: false
---

# What is `rinit`?

In a nutshell, an init system that aims to be simple, with sane default, while
also being memory-safe. It is written in Rust, a modern systems programming
language designed to prevent most of problems that arise when coding in C or
C++.

